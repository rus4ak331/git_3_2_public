step = int(input('Введіть кількість сходинок: '))

def number_of_ways(number_of_steps):
    if number_of_steps == 1:
        return 1
    
    elif number_of_steps == 2:
        return 2

    else:
        return number_of_ways(number_of_steps - 1) + number_of_ways(number_of_steps - 2)

print(number_of_ways(step))