def quadratic_equation(a, b, c):
    x_1 = None
    x_2 = None

    def calc_rezult():
        nonlocal x_1
        nonlocal x_2
        discriminant = (b ** 2) - (4*a*c)

        if discriminant < 0:
            return

        elif discriminant == 0:
            x_1 = -b / (2*a)

        elif discriminant > 0:
            x_1 = (-b + (discriminant ** 0.5)) / (2*a)
            x_2 = (-b - (discriminant ** 0.5)) / (2*a)
           
    calc_rezult()
    print(f'x1 - {x_1}, x2 - {x_2}')

a = float(input('Enter number a: '))
b = float(input('Enter number b: '))
c = float(input('Enter number c: '))

quadratic_equation(a, b, c)
